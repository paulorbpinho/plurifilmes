export class Movie {

  public title: string;
  public original_title: string;
  public genre: string;
  public homepage: string;
  public overview: string;
  public production_companies: string;
  public poster: string;
  public release_date: Date;
  public id: string;

  constructor() { }

  getReleaseDate() {
    return this.release_date.getDate() + '/' + this.release_date.getMonth() + '/' + this.release_date.getFullYear();
  }

  deserialize(data: any) {
    this.id = data.id;
    this.original_title = data.original_title;
    this.homepage = data.homepage;
    this.overview = data.overview;
    this.title = data.title;
    this.poster = data.poster_path;
    this.release_date = new Date(data.release_date);
    if(data.genres) {
      this.genre = data.genres.map(data => data.name).join(', ');
    }
    if(data.production_companies) {
      this.production_companies = data.production_companies.map(data => data.name).join(', ');
    }
    return this;
  }

}
