import { Component, OnInit } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../models/movie';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  public movie: Movie;
  //Para o mat-grid-list ser responsivo
  public responsive: boolean = true;
  public cols: number = 1;

  constructor(private movieService: MovieService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      this.movieService.getMovie( parseInt ( queryParams.get('id') ) ).add( () => {
        this.movie = new Movie().deserialize(this.movieService.data);
      });
    })
  }

}
