import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Service } from './service';

@Injectable({
  providedIn: 'root'
})

export class MovieService extends Service {

  public data: any;

  constructor(private httpClient: HttpClient) { super(); }

  getMovies(page: number = 1) {
    return this.httpClient.get( this.baseUrl+'/3/discover/movie?api_key=' + this.apiKey + '&language=pt-BR&sort_by=popularity.desc&include_adult=false&include_video=false&page='+page ).subscribe(
      data => {
        this.data = data;
      },
      error => {
        alert('Ocorreu um erro ao tentar recuperar a lista de filmes... Tente novamente mais tarde.');
      }
    );
  }

  getMovie(id: number) {
    return this.httpClient.get( this.baseUrl+'/3/movie/' + id + '?api_key=' + this.apiKey + '&language=pt-BR' ).subscribe(
      data => {
        this.data = data;
      },
      error => {
        alert('Ocorreu um erro ao tentar recuperar o filme... Tente novamente mais tarde.');
      }
    );
  }
}
