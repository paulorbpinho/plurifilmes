import { Component, OnInit } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { Movie } from '../models/movie';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})

export class MovieListComponent implements OnInit {

  public movies: Movie[];
  public page: number = 1;
  //Para o mat-grid-list ser responsivo
  public responsive: boolean = true;
  public cols: number = 1;
  public pages: number = 0;

  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.getMovies(this.page);
  }

  getMovies(page: number) {
    this.movieService.getMovies(page).add( () => {
      this.pages = this.movieService.data.total_pages;
      this.movies = this.movieService.data.results.map( data => new Movie().deserialize(data) ); 
    });
  }

  setPage(page: number) {
    this.page = page;
    this.getMovies(this.page);
  }

}
